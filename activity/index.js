const express = require('express');
const app = express();
const port = 8080;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

let users = [];

app.get('/home', (request,response) => {
	response.send("Welcome to the homepage");
})
app.get('/users', (request,response) => {
	response.send(users);
})

app.delete('/delete-user', (request,response) => {

	let message;
		for(let i = 0; i < users.length; i++)
		if(request.body.username == users[i].username){
			let index = users.indexOf(request.body.username)
			users.splice(index,1);

			message = `The ${request.body.username} has been deleted!`
		} else {
			message = "No username found, deletion is unsuccesful!"
		}
	response.send(message);
})


app.post('/adduser', (request,response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} succesfully registered!`)
	}else {
		response.send("Please input BOTH username and password")
	}
	
})
app.listen(port, () => console.log(`Server running at port ${port}`))

